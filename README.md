In this repo you can find a Youtube Downloader/Player

It was made in Visual Studio Community 2015
Windows Forms / C#.NET

The way it works is pretty simple.

1. You need to provide the program with an URL of a clip. You can do it either by writing it in a text box or clicking a YT movie in a browser.
2. Next, you must confirm that you would like to download a picked clip by clicking Download
3. Last step is pointing the program to specified directory, and choosing whether you want to download a movie or just an audio ( .mp3 .wav .avi .mp4).