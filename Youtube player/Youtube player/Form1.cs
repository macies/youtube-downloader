﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using YoutubeExtractor;

namespace Youtube_player
{
    public partial class Form1 : Form
    {
        Downloader downloader;
        Converter converter;
        bool isDownloadingFlag = false;
        List<DownloadRequest> downloadRequests;


        public Form1()
        {
            InitializeComponent();

            downloadRequests = new List<DownloadRequest>();

            converter = new Converter();
            converter.ConvertingFinished += OnConvertingFinished;

            downloader = new Downloader(progressBar1);

            saveFileDialog1.Filter = "Plik mp3 (*.mp3)|*.mp3|Plik wav (*.wav)|*.wav|Plik mp4 (*.mp4)|*.mp4|Plik avi (*.avi)|*.avi";

            FormBorderStyle = FormBorderStyle.Fixed3D;
            MinimizeBox = false;
            MaximizeBox = false;

            webBrowser1.Url = new Uri("http://www.youtube.pl");
            webBrowser1.ScriptErrorsSuppressed = true;
            webBrowser1.Navigating += WebBrowser1_Navigating;
            label1.Text = "";

        }

        private void WebBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            string link = webBrowser1.Url.ToString();
            string temp;
            if (DownloadUrlResolver.TryNormalizeYoutubeUrl(link, out temp))
                textBox1.Text = link;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            isDownloadingFlag = true;
            button1.Enabled = false;

            Task.Run(() => DownloadAndConvert(downloadRequests[0].info));


        }

        private void DownloadAndConvert(IEnumerable<VideoInfo> videoInfos)
        {

            label1.Invoke((Action)(() => { label1.Text = "Downloading"; }));

            string inputPath = downloader.Download(videoInfos);
            label1.Invoke((Action)(() => { label1.Text = "Converting"; }));

            string outputPath = downloadRequests[0].path;
            string extension = outputPath.Substring(outputPath.Length - 3, 3);

            converter.Convert(extension, inputPath, outputPath);
        }

        private void OnConvertingFinished(object sender, EventArgs e)
        {
            downloadListBox.Invoke((Action)(() => { downloadListBox.Items.RemoveAt(0); }));
            label1.Invoke((Action)(() => { label1.Text = ""; }));
            downloadRequests.RemoveAt(0);

            if (downloadRequests.Count > 0 && isDownloadingFlag == true)
            {
                Task.Run(() => DownloadAndConvert(downloadRequests[0].info));
            }
            else
            {
                isDownloadingFlag = false;
                button1.Invoke((Action)(() => { button1.Enabled = true; }));
            }
        }

        private void addToList_button_Click(object sender, EventArgs e)
        {

            string link = textBox1.Text;
            string outputPath;
            if (CheckIfLinkIsValid(link))
            {

                DialogResult result = saveFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    outputPath = saveFileDialog1.FileName;
                    saveFileDialog1.FileName = "";
                }
                else return;

                IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(link, false);
                DownloadRequest req = new DownloadRequest(link, outputPath, videoInfos);


                downloadRequests.Add(req);
                downloadListBox.Items.Add(req.title);




            }
        }

        private void removeRequest_button_Click(object sender, EventArgs e)
        {
            if (downloadListBox.SelectedIndex > 0 || isDownloadingFlag == false)
            {
                int index = downloadListBox.SelectedIndex;
                downloadListBox.Items.RemoveAt(index);
                downloadRequests.RemoveAt(index);

            }
        }

        private void moveUpReq_button_Click(object sender, EventArgs e)
        {
            if ((downloadListBox.SelectedIndex > 0) && (downloadListBox.SelectedIndex > 1 || isDownloadingFlag == false))
            {
                int index = downloadListBox.SelectedIndex;
                string item = (string)downloadListBox.Items[index];

                downloadListBox.Items[index] = downloadListBox.Items[index - 1];
                downloadListBox.Items[index - 1] = item;

                DownloadRequest req = downloadRequests[index];
                downloadRequests[index] = downloadRequests[index - 1];
                downloadRequests[index - 1] = req;

            }
        }

        private void moveDownReq_button_Click(object sender, EventArgs e)
        {
            if ((downloadListBox.SelectedIndex < downloadListBox.Items.Count - 2) && (downloadListBox.SelectedIndex > 0 || isDownloadingFlag == false))
            {

                int index = downloadListBox.SelectedIndex;
                string item = (string)downloadListBox.Items[index];

                downloadListBox.Items[index] = downloadListBox.Items[index + 1];
                downloadListBox.Items[index + 1] = item;

                DownloadRequest req = downloadRequests[index];
                downloadRequests[index] = downloadRequests[index - 1];
                downloadRequests[index + 1] = req;
            }
        }

        private void stopDownload_button_Click(object sender, EventArgs e)
        {
            isDownloadingFlag = false;
        }

        private bool CheckIfLinkIsValid(string link)
        {
            if (string.IsNullOrEmpty(link))
            {
                MessageBox.Show("Type in an URL");
                return false;

            }

            string temp;

            if (!DownloadUrlResolver.TryNormalizeYoutubeUrl(link, out temp))
            {
                MessageBox.Show("Wrong URL");
                return false;
            }
            return true;
        }

    }


}
