﻿using System;
using System.Diagnostics;
using System.IO;

namespace Youtube_player
{
    public class Converter
    {
        public delegate void OnConvertingFinished(object sender,EventArgs args);
        public event OnConvertingFinished ConvertingFinished;

        public void Convert(string extension, string inputPath, string outputPath)
        {
            File.Delete(Path.Combine(Directory.GetCurrentDirectory(), "output." + extension));
            string command = @"bin\ffmpeg -i " + "\"" + @"bin\ video.mp4" + "\"" + " output." + extension;

            var ProcessInfo = new ProcessStartInfo("cmd.exe", "/c" + command);
            ProcessInfo.WindowStyle = ProcessWindowStyle.Hidden;
            Process process = Process.Start(ProcessInfo);

            while (true)
            {
                try
                {
                    Process.GetProcessById(process.Id);
                }
                catch (ArgumentException)
                {
                    break;
                }

            }

            if (File.Exists(outputPath))  File.Delete(outputPath);
            File.Move(Path.Combine(Directory.GetCurrentDirectory(), "output." + extension), outputPath);
            File.Delete(Path.Combine(Directory.GetCurrentDirectory(), inputPath));
            ConvertingFinished(this, new EventArgs());
        }


    }
}
