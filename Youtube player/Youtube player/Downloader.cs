﻿using System.Collections.Generic;
using System.Linq;
using YoutubeExtractor;
using System.IO;
using System.Windows.Forms;
using System;

namespace Youtube_player
{
    public class Downloader
    {
        private ProgressBar progressBar;
               
        public Downloader(ProgressBar bar)
        {
            progressBar = bar;


        }

        public string Download(IEnumerable<VideoInfo> videoInfos)
        {
            string videoPath;

            VideoInfo video = videoInfos
                .First(info => info.VideoType == VideoType.Mp4 && info.Resolution == 360);

            if (video.RequiresDecryption)
            {
                DownloadUrlResolver.DecryptDownloadUrl(video);
            }
            videoPath = Path.Combine(Directory.GetCurrentDirectory(), @"bin/ " + "video" + video.VideoExtension);
            var videoDownloader = new VideoDownloader(video, videoPath);

            progressBar.Invoke((Action)(() => { progressBar.Visible = true; }));
            videoDownloader.DownloadProgressChanged += (sender, args) => progressBar.Invoke((Action)(() => { progressBar.Value = (int)args.ProgressPercentage; }));
           
            videoDownloader.Execute();

            progressBar.Invoke((Action)(() => { progressBar.Visible = false; }));
            progressBar.Invoke((Action)(() => { progressBar.Value = 0; }));

            string path = @"bin/" + "video" + video.VideoExtension;
            return path;

        }
    }
}
