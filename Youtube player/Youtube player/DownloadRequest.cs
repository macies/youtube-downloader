﻿using System.Collections.Generic;
using YoutubeExtractor;

namespace Youtube_player
{
    public class DownloadRequest
    {
        public string link;
        public string path;
        public IEnumerable<VideoInfo> info;
        public string title;
     

        public DownloadRequest(string l,string p, IEnumerable<VideoInfo> inf)
        {
            link = l;
            path = p;
            info = inf;

            foreach (VideoInfo vInf in inf)
            {
                if (vInf.Title != null)
                    title = vInf.Title;
            }

        }
    }
}