﻿namespace Youtube_player
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.stopDownload_button = new System.Windows.Forms.Button();
            this.downloadListBox = new System.Windows.Forms.ListBox();
            this.addToList_button = new System.Windows.Forms.Button();
            this.removeRequest_button = new System.Windows.Forms.Button();
            this.moveUpReq_button = new System.Windows.Forms.Button();
            this.moveDownReq_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(851, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Download";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(15, 9);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(480, 20);
            this.textBox1.TabIndex = 1;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(129, 35);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(577, 23);
            this.progressBar1.TabIndex = 2;
            this.progressBar1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "State_Descripton_label";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(3, 62);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(703, 687);
            this.webBrowser1.TabIndex = 4;
            // 
            // stopDownload_button
            // 
            this.stopDownload_button.Location = new System.Drawing.Point(682, 9);
            this.stopDownload_button.Name = "stopDownload_button";
            this.stopDownload_button.Size = new System.Drawing.Size(152, 23);
            this.stopDownload_button.TabIndex = 5;
            this.stopDownload_button.Text = "Stop";
            this.stopDownload_button.UseVisualStyleBackColor = true;
            this.stopDownload_button.Click += new System.EventHandler(this.stopDownload_button_Click);
            // 
            // downloadListBox
            // 
            this.downloadListBox.FormattingEnabled = true;
            this.downloadListBox.Location = new System.Drawing.Point(712, 62);
            this.downloadListBox.Name = "downloadListBox";
            this.downloadListBox.Size = new System.Drawing.Size(294, 680);
            this.downloadListBox.TabIndex = 6;
            // 
            // addToList_button
            // 
            this.addToList_button.Location = new System.Drawing.Point(515, 9);
            this.addToList_button.Name = "addToList_button";
            this.addToList_button.Size = new System.Drawing.Size(141, 23);
            this.addToList_button.TabIndex = 7;
            this.addToList_button.Text = "Add to list";
            this.addToList_button.UseVisualStyleBackColor = true;
            this.addToList_button.Click += new System.EventHandler(this.addToList_button_Click);
            // 
            // removeRequest_button
            // 
            this.removeRequest_button.Location = new System.Drawing.Point(712, 36);
            this.removeRequest_button.Name = "removeRequest_button";
            this.removeRequest_button.Size = new System.Drawing.Size(75, 23);
            this.removeRequest_button.TabIndex = 8;
            this.removeRequest_button.Text = "Remove";
            this.removeRequest_button.UseVisualStyleBackColor = true;
            this.removeRequest_button.Click += new System.EventHandler(this.removeRequest_button_Click);
            // 
            // moveUpReq_button
            // 
            this.moveUpReq_button.Location = new System.Drawing.Point(826, 36);
            this.moveUpReq_button.Name = "moveUpReq_button";
            this.moveUpReq_button.Size = new System.Drawing.Size(75, 23);
            this.moveUpReq_button.TabIndex = 9;
            this.moveUpReq_button.Text = "Move Up";
            this.moveUpReq_button.UseVisualStyleBackColor = true;
            this.moveUpReq_button.Click += new System.EventHandler(this.moveUpReq_button_Click);
            // 
            // moveDownReq_button
            // 
            this.moveDownReq_button.Location = new System.Drawing.Point(931, 36);
            this.moveDownReq_button.Name = "moveDownReq_button";
            this.moveDownReq_button.Size = new System.Drawing.Size(75, 23);
            this.moveDownReq_button.TabIndex = 10;
            this.moveDownReq_button.Text = "Move Down";
            this.moveDownReq_button.UseVisualStyleBackColor = true;
            this.moveDownReq_button.Click += new System.EventHandler(this.moveDownReq_button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 761);
            this.Controls.Add(this.moveDownReq_button);
            this.Controls.Add(this.moveUpReq_button);
            this.Controls.Add(this.removeRequest_button);
            this.Controls.Add(this.addToList_button);
            this.Controls.Add(this.downloadListBox);
            this.Controls.Add(this.stopDownload_button);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Youtube downloader";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button stopDownload_button;
        private System.Windows.Forms.ListBox downloadListBox;
        private System.Windows.Forms.Button addToList_button;
        private System.Windows.Forms.Button removeRequest_button;
        private System.Windows.Forms.Button moveUpReq_button;
        private System.Windows.Forms.Button moveDownReq_button;
    }
}

